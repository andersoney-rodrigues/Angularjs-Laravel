import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-conteudo',
  templateUrl: './conteudo.component.html',
  styleUrls: ['./conteudo.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ConteudoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
