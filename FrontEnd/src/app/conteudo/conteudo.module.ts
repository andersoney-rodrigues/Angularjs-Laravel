import { ConteudoComponent } from './conteudo.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './/app-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  declarations: [ConteudoComponent, LoginComponent, HomeComponent],
  exports: [ConteudoComponent]
})
export class ConteudoModule { }
