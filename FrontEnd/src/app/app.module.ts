import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ConteudoModule } from './conteudo/conteudo.module';
import { LayoutModule } from './layout/layout.module';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    LayoutModule,
    ConteudoModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
